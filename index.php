<?php
spl_autoload_register(function ($class_name) {
    include './classes/' . $class_name . '.php';
});
?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>To-Do</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/app.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type='text/javascript' src="js/todo.js"></script>
</head>
<body>
<div class="container">
    <!-- The Modal -->
    <div class="modal fade" id="changeModal">
        <div class="modal-dialog modal-dialog-centered" >
            <div class="modal-content" >
                <form id="f_change">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-name" id="modalName">Edit To-Do Item</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group" id="first">
                            <div class="input-group ">
                                <label class="label1" for="name">Name</label>
                                <input type="text" class="form-control" name="new_name" id="new_name" value="" required
                                       title="Item Name" placeholder="Item Name" maxlength="50"
                                />
                            </div>
                            <br>
                            <div class="input-group ">
                                <label class="label1" for="name">Description</label>
                                <textarea id="new_description" name="new_description" rows="4" maxlength="10000"
                                required title="Item Description" placeholder="Item Description" class="form-control">
                                </textarea>
                            </div>
                            <br>
                            <div class="input-group ">
                                <label class="label1" for="due_date">Due Date</label>
                                <input type="text" class="form-control" name="new_due_date" id="new_due_date" value="" required
                                       pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d"
                                       title="mm/mm/yyyy" placeholder="mm/dd/yyyy" maxlength="10"
                                />
                            </div>
                        </div>

                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="btn_save">Save</button>
                        <!--button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button -->
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- The Details Modal -->
    <div class="modal fade" id="viewModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">To-Do Item Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div><b>Name</b></div>
                        <span id="vName"></span>
                        <div><b>Description</b></div>
                        <span id="vDescription"></span>
                        <div><b>Status</b></div>
                        <span id="vStatus"></span>
                        <div><b>Due Date</b></div>
                        <span id="vDue_date"></span>
                        <div><b>Completed</b></div>
                        <span id="vCompleted"></span>
                        <div><b>Created</b></div>
                        <span id="vCreated"></span>
                        <div><b>Updated</b></div>
                        <span id="vUpdated"></span>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                    </div>
            </div>
        </div>
    </div>
    <h2>OnlineMedEd To-Do List</h2>
    <div id="err" class="alert alert-info" style="width: 650px">Select a To-Do Item to enable Actions</div>
</div>
<div class="container" id="btnblock" style="margin-bottom: 5px;">
    <button id="btn_add" class="btn btn-group-sm btn-primary" ><i class="fa fa-plus"></i> Add</button>
    <button id="btn_edit" class="btn btn-group-sm btn-primary" disabled ><i class="fa fa-edit"></i> Edit</button>
    <button id="btn_view" class="btn btn-group-sm btn-info" disabled ><i class="fa fa-eye"></i> Details</button>
    <button id="btn_complete" class="btn btn-group-sm btn-success" disabled ><i class="fa fa-check"></i> Complete</button>
    <button id="btn_delete" class="btn btn-group-sm btn-danger" disabled ><i class="fa fa-trash"></i> Delete</button>
</div>
<div class="container" id="todo">
    <table class="table table-bordered" id="list" width="700px">
        <tr>
            <th width="60px" class="text-center">#</th>
            <th class="text-center">Name</th>
            <th width="100px" class="text-center">Due Date</th>
            <th width="100px" class="text-center">Completed</th>
            <th width="100px" class="text-center">Status</th>
        </tr>
        <tr>
            <td width="60px" class="text-center"></td>
            <td class="text-center"></td>
            <td width="100px" class="text-center"></td>
            <td width="100px" class="text-center"></td>
            <td width="100px" class="text-center"></td>
        </tr>
    </table>
</div>
</body>
</html>

