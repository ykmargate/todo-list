<?php
class Db{
    private $connection;
    public function __construct(PDO $connection = null){
        $this->connection = $connection;
        if ($this->connection === null) {
            $env = dirname(__FILE__)."/../.env";
            $dsn = parse_ini_file( $env, true )['DSN'];
            $this->connection = new PDO(
                $dsn['dbtype'].':host='.$dsn['host'].';dbname='.$dsn['database'],
                $dsn['username'],
                $dsn['password']
            );

            $this->connection->setAttribute(
                PDO::ATTR_ERRMODE,
                PDO::ERRMODE_EXCEPTION
            );
        }
    }

    public function randomString(){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 20; $i++) {
            $randstring = $randstring.$characters[rand(0, strlen($characters))];
        }
        return $randstring;
    }

    public function authenticate($login){
        $stmt = $this->connection->prepare('SELECT 1 as found FROM weblogins WHERE user = :user and password = :password'
        );
        $stmt->execute($login);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function queryToJson($sql, $params=null){
        $stmt = $this->connection->prepare($sql);
        if(is_null($params)){
            $stmt->execute();
        }
        else{
            $stmt->execute($params);
        }
        return json_encode( $stmt->fetchAll(PDO::FETCH_ASSOC) );
    }
    public function queryAll($sql, $params=null){
        $stmt = $this->connection->prepare($sql);
        if(is_null($params)){
            $stmt->execute();
        }
        else{
            $stmt->execute($params);
        }
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function queryOne($sql, $params=null){
        $stmt = $this->connection->prepare($sql);
        if(is_null($params)){
            $stmt->execute();
        }
        else{
            $stmt->execute($params);
        }
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function queryInsert($sql, $params=null){
        $stmt = $this->connection->prepare($sql);
        if(is_null($params)){
            $stmt->execute();
        }
        else{
            $stmt->execute($params);
        }
        return $this->connection->lastInsertId();
    }

    public function queryUpdate($sql, $params=null){
        try {
            $stmt = $this->connection->prepare($sql);
            if(is_null($params)){
                $stmt->execute();
            }
            else{
                $stmt->execute($params);
            }
        }catch(PDOException $e) {
            return $e->getMessage();
        }
        return $stmt->rowCount();
    }

    public function queryDelete($sql, $params=null){
        try {
            $stmt = $this->connection->prepare($sql);
            if(is_null($params)){
                $stmt->execute();
            }
            else{
                $stmt->execute($params);
            }
        }catch(PDOException $e) {
            return $e->getMessage();
        }
        return $stmt->rowCount();
    }
}