<?php
spl_autoload_register(function ($class_name) {
    include './classes/' . $class_name . '.php';
});

$db = new Db();
$action = $_REQUEST['action'];
$status = array('code'=>0, 'message'=>'');

switch ($action){
    case 'read':
        try {
            $sql = 'SELECT id, name, description, DATE_FORMAT(created, \'%m/%d/%Y %h:%i %p\') as created,'
                    .' DATE_FORMAT(updated, \'%m/%d/%Y %h:%i %p\') as updated,'
                    .' DATE_FORMAT(due_date, \'%m/%d/%Y\') as due_date,'
                    .'DATE_FORMAT(completed, \'%m/%d/%Y\') as completed,'
                    .'status FROM to_do WHERE status < 2 order by due_date, name';
            $response = $db->queryAll($sql, $params);
            $status['message'] = 'Success';
        }
        catch(PDOException $e){
            $status['code'] = $e->getCode();
            $status['message'] = $e->getMessage();
        }
        break;
    case 'add':
        try {
            $sql = "INSERT INTO to_do (name, description, due_date) values(:name, :description, STR_TO_DATE(:due_date, \"%m/%d/%Y\"))";
            $params = array(
                ':name' => $_REQUEST['name'],
                ':description' => $_REQUEST['description'],
                ':due_date' => $_REQUEST['due_date']
            );
            $count = $db->queryInsert($sql, $params);
            if (is_numeric($count)) {
                $response = array('count' => $count);
            } else {
                $status['code'] = 1;
                $status['message'] = $count;
                $response = array('count' => 0);
            }
        }
        catch(PDOException $e){
            $status['code'] = $e->getCode();
            $status['message'] = $e->getMessage();
        }
        break;
    case 'save':
        try {
            $sql = "UPDATE to_do SET name = :name, description = :description, due_date = STR_TO_DATE(:due_date, \"%m/%d/%Y\"), updated = now() WHERE id = :id";
            $params = array(
                ':id' => $_REQUEST['id'],
                ':name' => $_REQUEST['name'],
                ':description' => $_REQUEST['description'],
                ':due_date' => $_REQUEST['due_date']
            );
            $count = $db->queryUpdate($sql, $params);
            if (is_numeric($count)) {
                $response = array('count' => $count);
            } else {
                $status['code'] = 1;
                $status['message'] = $count;
                $response = array('count' => 0);
            }
        }
        catch(PDOException $e){
            $status['code'] = $e->getCode();
            $status['message'] = $e->getMessage();
        }
        break;
    case 'delete':
            try {
                $sql = "UPDATE to_do SET status = 2, updated = NOW() WHERE id = :id";
                $params = array(
                    ':id' => $_REQUEST['id']
                );
                $count = $db->queryUpdate($sql, $params);
                if (is_numeric($count)) {
                    $response = array('count' => $count);
                } else {
                    $status['code'] = 1;
                    $status['message'] = $count;
                    $response = array('count' => 0);
                }
        }
            catch(PDOException $e){
                $status['code'] = $e->getCode();
                $status['message'] = $e->getMessage();
                $response = array('count' => 0);
            }
        break;
    case 'completed':
        try {
            $sql = "UPDATE to_do SET status = 1, updated = NOW(), completed = NOW() WHERE id = :id";
            $params = array(
                ':id' => $_REQUEST['id']
            );
            $count = $db->queryUpdate($sql, $params);
            if (is_numeric($count)) {
                $response = array('count' => $count);
            } else {
                $status['code'] = 1;
                $status['message'] = $count;
                $response = array('count' => 0);
            }
        }
        catch(PDOException $e){
            $status['code'] = $e->getCode();
            $status['message'] = $e->getMessage();
            $response = array('count' => 0);
        }
        break;
    default:
        break;
}
$db = null;
$response = array(
    'status'=> $status,
    'response' => $response,
);
$response = json_encode($response);
header('Content-Type: application/json');
echo $response;
?>
