function read(){
    $.post("todo_api.php",
        {
            action: 'read'
        }).done(
        function(data){
            if (data.status.code == 0) {
                showresult(data.response);
            }
            else {
                showMessage(data.status.message, errorClass);
            }
        }
    ).fail(function(jqXHR, textStatus, errorThrown){
            showMessage(jqXHR.status+" {"+errorThrown+"}", errorClass);
        }
    );
}
function showresult(data) {
    readData = data;
    let appHTML = '<table class="table table-bordered" id="list" style="width: 650px">' +
        '<tr class="header">' +
        '<th style="width: 60px" class="text-center">#</th>' +
        '<th class="text-center">Name</th>' +
        '<th style="width: 100px" class="text-center">Due Date</th>' +
        '<th style="width: 100px" class="text-center">Completed</th>' +
        '<th style="width: 60px" class="text-center">Status</th>' +
        '</tr>';
    if (data.length > 0) {
        $.each(data, function (i, item) {
            let Difference_In_Days = overdue(item.due_date);
            if(item.completed == null){
                item.completed = '';
            }
            if(item.status == '1'){
                square = greenSquare;
            }
            else if (item.status == '0' && Difference_In_Days >= 0){
                square = yellowSquare;
            }
            else if (item.status == '0' && Difference_In_Days < 0){
                square = redSquare;
            }
            appHTML += '<tr>' +
                '<td style="width: 60px" class="text-right">' + (i + 1) + '</td>' +
                '<td class="text-left">' + item.name + '</td>' +
                '<td style="width: 100px" class="text-center">' + item.due_date + '</td>' +
                '<td style="width: 100px" class="text-center">' + item.completed + '</td>' +
                '<td style="width: 60px"" class="text-center">' + square + '</td>' +
                '</tr>';
        })
    }
    else {
        appHTML += '<tr>' +
            '<td colspan="4" class="text-center">No data</td>' +
            '</tr>';
    }
    appHTML += "</table>";
    $("#btn_edit, #btn_delete, #btn_view, #btn_complete").prop('disabled', true);
    $("#todo").empty().append(appHTML);
    $('[data-toggle="tooltip"]').tooltip();
    $("#list").on("click", "tr", function () {
        $(this).closest("tr").siblings().removeClass("highlighted");
        if( !$(this).hasClass( "header" ) ){
            if(data.length > 0){
                $(this).toggleClass("highlighted");
            }
        }
        if( $(this).hasClass( "highlighted" ) ){
            let f = $(this).find("td");
            fields.ind = f[0].innerText;
            fields.name = f[1].innerText;
            fields.id = readData[fields.ind - 1].id;
            $("#btn_edit, #btn_delete, #btn_view, #btn_complete").prop('disabled', false);
            if(readData[fields.ind - 1].status == '1'){
                $("#btn_edit, #btn_complete").prop('disabled', true);
            }
        }
        else {
            $("#btn_edit, #btn_delete, #btn_view, #btn_complete").prop('disabled', true);
            fields = new Object();
        };
    });
}

function showMessage(message, className) {
    $("#err").removeClass().addClass(className).text(message).delay(delayMS).queue(function(next){
        $(this).text('Select a To-Do Item to enable Actions').removeClass().addClass(infoClass);
        next();
    });
}

function overdue(due_date) {
    let due = new Date(due_date);
    let Difference_In_Time = due.getTime() - present_date.getTime();
    // To calculate the no. of days between two dates
    return Math.round(Difference_In_Time / (1000 * 3600 * 24));
}

let fields = new Object();
let action = '';
let readData;
const present_date = new Date();
const delayMS = 5000;
const errorClass = 'alert alert-danger';
const infoClass = 'alert alert-info';
const successClass = 'alert alert-success';
const greenSquare = '<img alt="status icon"  src="images/square-green-24.png" data-toggle="tooltip" data-placement="top" title="Compleded">';
const yellowSquare = '<img alt="status icon"  src="images/square-yellow-24.png" data-toggle="tooltip" data-placement="top" title="Submitted">';
const redSquare = '<img alt="status icon"  src="images/square-red-24.png" data-toggle="tooltip" data-placement="top" title="Overdue">';
let square = '';


$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    read();

    $("#btn_add").click(function (){
        $("#modalName").text('Add To-Do Item');
        $("#new_name").val('');
        $("#new_description").val('');
        $("#new_due_date").val('');
        $("#changeModal").modal();
        $("#btn_save").click(function(event){
            if($(this).closest('form')[0].checkValidity()){
                event.preventDefault();
                $("#changeModal").modal('toggle');
                $.post("todo_api.php",
                    {
                        action: 'add',
                        name: $("#new_name").val(),
                        description: $("#new_description").val(),
                        due_date: $("#new_due_date").val()
                    }).done(
                    function(data){
                        if (data.status.code == 0) {
                            showMessage('New To-Do item has been added', successClass);
                            read();
                        }
                        else {
                            showMessage(data.status.message, errorClass);
                        }
                    }
                ).fail(function(jqXHR, textStatus, errorThrown){
                        showMessage(jqXHR.status+" {"+errorThrown+"}", errorClass);
                    }
                );
            }
        })
    });

    $("#btn_edit").click(function (){
        $("#modalName").text('Edit To-Do Item');
        let rows = $("#list tr.highlighted");
        let f = $(rows).find("td");
        let ind = f[0].innerText -1;
        let name = readData[ind].name;
        let description = readData[ind].description;
        let due_date = readData[ind].due_date
        $("#new_name").val(name);
        $("#new_description").val(description);
        $("#new_due_date").val(due_date);
        $("#changeModal").modal();
        $("#btn_save").click(function(event){
            if($(this).closest('form')[0].checkValidity()){
                event.preventDefault();
                $("#changeModal").modal('toggle');
                $.post("todo_api.php",
                    {
                        action: 'save',
                        name: $("#new_name").val(),
                        description: $("#new_description").val(),
                        due_date: $("#new_due_date").val(),
                        id: readData[ind].id
                    }).done(
                    function(data){
                        if (data.status.code == 0) {
                            showMessage('To-Do item has been updated', successClass);
                            read();
                        }
                        else {
                            showMessage(data.status.message, errorClass);
                        }
                    }
                ).fail(function(jqXHR, textStatus, errorThrown){
                        showMessage(jqXHR.status+" {"+errorThrown+"}", errorClass);
                    }
                );
            }
        })
    });

    $("#btn_view").click(function(){
        let rows = $("#list tr.highlighted");
        let f = $(rows).find("td");
        let ind = f[0].innerText -1;
        let name = readData[ind].name;
        let description = readData[ind].description;
        let status = readData[ind].status;
        let created = readData[ind].created;
        let updated = readData[ind].updated;
        let due_date = readData[ind].due_date;
        let completed = readData[ind].completed;
        let Difference_In_Days = overdue(due_date);
        if(!completed){
            completed = 'Not Completed';
        }
        if(status == '1'){
            status = 'Completed';
        }
        else if (status == '0' && Difference_In_Days >= 0){
            status = 'Submitted';
        }
        else if (status == '0' && Difference_In_Days < 0){
            status = 'Overdue';
        }

        $("#vName").text(name);
        $("#vDescription").text(description);
        $("#vStatus").text(status);
        $("#vCreated").text(created);
        $("#vUpdated").text(updated);
        $("#vDue_date").text(due_date);
        $("#vCompleted").text(completed);
        $("#viewModal").modal();

    });

    $("#btn_delete").click(function (){
        let rows = $("#list tr.highlighted");
        let f = $(rows).find("td");
        let ind = f[0].innerText -1;
        $.post("todo_api.php",
            {
                action: 'delete',
                id: readData[ind].id
            }).done(
            function(data){
                if (data.status.code == 0) {
                    showMessage('Document has been deleted', successClass);
                    readData.splice(ind,1);
                    showresult(readData);
                }
                else {
                    showMessage(data.status.message, errorClass);
                }
            }).fail(
            function(jqXHR, textStatus, errorThrown){
                showMessage(jqXHR.status+" {"+errorThrown+"}", errorClass);
            }
        );
    });

    $("#btn_complete").click(function (){
        let rows = $("#list tr.highlighted");
        let f = $(rows).find("td");
        let ind = f[0].innerText -1;
        $.post("todo_api.php",
            {
                action: 'completed',
                id: readData[ind].id
            }).done(
            function(data){
                if (data.status.code == 0) {
                    showMessage('To-Do has been marked as completed', successClass);
                    read();
                }
                else {
                    showMessage(data.status.message, errorClass);
                }
            }).fail(
            function(jqXHR, textStatus, errorThrown){
                showMessage(jqXHR.status+" {"+errorThrown+"}", errorClass);
            }
        );
    });

});
