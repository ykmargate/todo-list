/*
 Source Server         : codesamples
 Source Server Type    : MySQL
 Source Server Version : 50647
 Source Host           : localhost:3306
 Source Schema         : flwebdev_edmed
 Target Server Type    : MySQL
 Target Server Version : 50647
 File Encoding         : 65001
 Date: 03/05/2020 20:11:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for to_do
-- ----------------------------
DROP TABLE IF EXISTS `to_do`;
CREATE TABLE `to_do`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `description` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` int(11) NULL DEFAULT 0,
  `created` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `due_date` date NULL DEFAULT NULL,
  `completed` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of to_do
-- ----------------------------
INSERT INTO `to_do` VALUES (1, 'To-Do title 1', 'To-Do Description 1', 1, '2020-05-02 20:03:31', '2020-05-03 14:01:33', '2020-05-12', '2020-05-25');
INSERT INTO `to_do` VALUES (2, 'new To-Do title 2', 'new To-Do Description 2', 0, '2020-05-03 00:07:19', '2020-05-03 16:32:02', '2020-05-25', NULL);
INSERT INTO `to_do` VALUES (3, 'new To-Do title 3', 'new To-Do Description 3', 1, '2020-05-03 00:14:38', '2020-05-03 16:29:34', '2020-05-25', '2020-05-01');
INSERT INTO `to_do` VALUES (4, 'To-Do title 4', 'To-Do Description 4', 1, '2020-05-03 00:24:44', '2020-05-03 00:24:44', '2020-05-02', '2020-05-03');
INSERT INTO `to_do` VALUES (5, 'To-Do title 5', 'To-Do Description 5 To-Do Description 5 To-Do Description 5 To-Do Description 5 To-Do Description 5', 0, '2020-05-03 14:53:49', '2020-05-03 17:07:06', '2020-05-02', NULL);
INSERT INTO `to_do` VALUES (6, 'Add new Title', 'Add new Description', 0, '2020-05-03 16:52:38', '2020-05-03 16:52:38', '2020-05-13', NULL);

SET FOREIGN_KEY_CHECKS = 1;
